package Endomondo;

use Modern::Perl;
use URI;
use LWP::Simple qw/get/;
use JSON qw/decode_json/;

sub login {
    my $invocant = shift;
    my $class = ref($invocant) || $invocant;

    my $auth = {};
    my $URL_AUTH = 'http://api.mobile.endomondo.com/mobile/auth';

    my $url = URI->new($URL_AUTH);
    $url->query_form(
        v => 2.4,
        action => 'PAIR',
        country => 'RU',
        deviceId => '77b95e39-0dff-4070-bf5b-06eb8afc284a',
        os => 'Android',
        appVersion => "8.7",
        appVariant => "M-Pro", # || Market
        osVersion => "2.3.7",
        model => "Legend",
        @_ # email, password
    );

    eval {
        my @content = split /\R/, (get($url) or die "Couldn't get it! $!");
        my $status = shift @content;
        die "Authentication error: $status" unless $status eq 'OK';
        %$auth = map { split /=/ } @content;
    };

    if ($@) {
        warn $@;
        return;
    }

    bless ($auth, $class);
}

sub make_request {
    my ($self, $url, $params) = @_;

    @$params{ 'authToken', 'language' } = ( $self->{authToken}, 'RU' );

    $url = URI->new($url);
    $url->query_form($params);

    $url;
}

sub get_workout_list {
    my $self = shift;
    my $URL_WORKOUTS = 'http://api.mobile.endomondo.com/mobile/api/workout/list';

    my $url = make_request($self, $URL_WORKOUTS, { maxResult => 40 });

    my $content = get($url) or die "Couldn't get it!";

    decode_json($content);
}

sub get_workout {
    my ($self, $id) = @_;

    my @workout;
    my $URL_TRACK = 'http://api.mobile.endomondo.com/mobile/readTrack';

    my $url = make_request($self, $URL_TRACK, { trackId => $id });
    eval {
        @workout = split /\R/, get($url) or die "Couldn't get it!";
        my $status = shift @workout;
        die "Status not ok!" unless $status eq 'OK';
    };

    if ($@) {
        warn $@;
        return;
    }

    \@workout;
}

sub get_hz_list {
    my $self = shift;
    my $URL_FRIENDS = 'http://api.mobile.endomondo.com/mobile/api/feed';

    my $url = make_request($self, $URL_FRIENDS);

    my $content = get($url) or die "Couldn't get it!";

    decode_json($content);
}

sub get_hz2_list {
    my $self = shift;
    my $URL_HZ2 = 'http://api.mobile.endomondo.com/mobile/api/workout/get';

    my $url = make_request($self, $URL_HZ2, {fields => 'basic,points,playlist,interval', workoutId => '184533859', deflate => 'false'});

    my $content = get($url) or die "Couldn't get it!";

    $content;
}

sub get_friend_list {
    my $self = shift;
    my $URL_FRIENDS = 'http://api.mobile.endomondo.com/mobile/friends';

    my $url = make_request($self, $URL_FRIENDS);

    my $content = get($url) or die "Couldn't get it!";

    $content;
}

1;
